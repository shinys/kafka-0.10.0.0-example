package sys.kafka.test;

import com.google.gson.reflect.TypeToken;
import sys.kafka.Consumer;

/**
 * ConsumerTest (UTF-8)
 * created : 2016. 2. 15
 * 
 * @golfzon.service 
 * @golfzon.group 
 * @golfzon.menu 
 * @author shinys
 */
public class ConsumerTest {
    public static void main(String[] args) throws Exception {
        
        String brokers = "192.168.204.126:9092,192.168.204.126:9093,192.168.204.126:9094";
        String groupId = "my-topic-group";
        String clientId = "my-topic-01";
        String topic = "my-topic";
        
        String data;
        try (Consumer c = new Consumer(brokers, topic, groupId, clientId)) {
            data = c.nextData(new TypeToken<String>(){}.getType());
            //session.timeout.ms 보다 긴 시간동안 poll() 하지 않은 경우 예외 확인.
            Thread.sleep(20000);
            data = c.nextData(String.class);
        }
        
        
    }

}
