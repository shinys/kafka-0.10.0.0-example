# README #

kafka_2.11-0.10.0.0 의 서버 configuration과 Producer , Consumer 예제 코드.
본 예제 코드는 3개의 partition을 가진 my-topic 이라는 topic을 생성하고 5개의 String 데이터를 카프카 브로커에 publishing 후 my-topic-group 이라는 Conumer로 subcribing 하는 예제 코드.
producer,consumer 코드외에 kafka.admin.AdminUtils 및 kafka.utils.ZkUtils 사용법을 참조할 수 있음.


![kafka_cluster_도식1.png](https://bitbucket.org/repo/6Gr8zo/images/934553130-kafka_cluster_%EB%8F%84%EC%8B%9D1.png)


![kafka_topic_replica_도식.png](https://bitbucket.org/repo/6Gr8zo/images/3548460640-kafka_topic_replica_%EB%8F%84%EC%8B%9D.png)


### Apache Kafka 0.10.0.0 변경점 ###
아파치 카프카 0.10.0.0 는 이전 버전 대비 인증, SSL레이어 추가 등 많은 변화가 있었습니다.
kafka topic 중심의 변경점은 다음과 같음.

변경

* 가장 큰 변화는 stream processing 지원 강화.( KStream , KTable )  http://kafka.apache.org/0100/documentation.html#streams

* 대량의 데이터를 손쉽게 카프카 내/외부로 전달하는 방법을 제공하기위한 Kafka Connect 기능 추가. http://kafka.apache.org/0100/documentation.html#connect

* 컨슈머의 exclude.internal.topics 지원 추가.

* ByteBuffer Serializer&Deserializer 추가.

* TopicMetadata,UpdateMetadata,OffsetCommit,OffsetFetch 등의 package 변경.

* KafkaStream 인터페이스는 Java 8 java.util.function 과 호환.

* Consumer.poll(0) 무효화

* Upgrade LZ4 to version 1.3 to avoid crashing with IBM Java 7.

